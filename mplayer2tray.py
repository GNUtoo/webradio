#!/usr/bin/env python2

#    Copyright (C) 2012 Denis 'GNUtoo' Carikli <GNUtoo@no-log.org>
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.


import pygtk
pygtk.require('2.0')
import gtk

import copy
import os
import re
import sys

from twisted.internet import gtk2reactor
gtk2reactor.install()
from twisted.internet import reactor
from twisted.internet.protocol import ProcessProtocol

import ConfigParser

icon = ""
if sys.argv[0] is "mplayer2tray.py":
    icon = "radio.png"
else:
    icon = sys.argv[0].replace("mplayer2tray.py", "radio.png")

gui = None

def log(msg):
    print "mplayer2tray: {0}".format(msg)

class Callbacks(ProcessProtocol):
    def __init__(self):
        self.errors = ["No stream found",
                       "Media data read failed",
                       "read error:: Resource temporarily unavailable",
                       "nop_streaming_read error",
                       "Resource temporarily unavailable",
                       "Error while reading network stream",
                       "mpg123: Can't rewind stream by","connect error:",
                       "Network is unreachable"]
        self.match = False

    def outReceived(self, data):
        for line in data.split("\n"):
            print 'stdout:', line

    def errReceived(self, data):
        self.match = False
        for line in data.split("\n"):
            print 'stderr:', line
        for error in self.errors:
            if re.search(error, data) is not None:
                self.match = True
        if self.match is True:
            log('match')
            gui.mplayer.radio(None,
                              gui.mplayer.saved_radio,
                              gui.mplayer.saved_label,
                              gui.mplayer.saved_Playlist)
        else:
            log('no match')

    def connectionMade(self):
        log('Started running subprocess')

    def processEnded(self, reason):
        log('Completed running subprocess')

    def send_command(self, command):
        self.transport.write(command)

class Communicator():
    def __init__(self):
        self.callback = Callbacks()
        self.process = reactor.spawnProcess(self.callback,
                                            'mplayer',
                                            ['mplayer', '-idle', '-quiet', '-slave'],
                                            env=os.environ)

    def run(self, command):
        log("running command {0}".format(command))
        self.callback.send_command(command)

class Mplayer():
    def __init__(self, bar):
        self.mplayer = Communicator()
        self.initial = True
        self.stoped = False
        self.paused = False
        self.bar = bar
        self.saved_label = None
        self.saved_radio = None
        self.saved_Playlist = None

    def radio(self, widget, radio, label = None, Playlist = None):
        self.saved_radio = copy.copy(radio)
        self.saved_Playlist = copy.copy(Playlist)

        if label is not None:
            self.saved_label = copy.copy(label)
            self.bar.statusIcon.set_tooltip(label)

        if self.stoped is True:
            self.mplayer = Communicator()
            self.stoped = False

        if Playlist is True:
                log("Loading playlist")
                if self.initial is True:
                    self.mplayer.run("loadlist " + radio + " 1\n")
                else:
                    self.mplayer.run("loadlist " + radio + "  \n")
        else:
                log("Loading file")
                if self.initial is True:
                    self.mplayer.run("loadfile " + radio + " 1\n")
                else:
                    self.mplayer.run("loadfile " + radio + "  \n")
        self.initial = False

    def quit(self, widget = None ):
        self.mplayer.run("quit \n")

    def stop(self, widget = None, label = None):
        if label is not None:
            self.saved_label = copy.copy(label)
            self.bar.statusIcon.set_tooltip(label)
        self.quit()
        self.stoped = True

    def pause(self, widget = None, label = None):
        if self.paused is False: #so we stop pausing(was paused)
            self.paused = True
            if label is not None:
                self.bar.statusIcon.set_tooltip(label)
        else: #so we pause(was already not paused)
            self.paused = False
            if label is not None:
                if self.saved_label is None:
                    self.bar.statusIcon.set_tooltip("radio")
                else:
                    self.bar.statusIcon.set_tooltip(self.saved_label)
        self.mplayer.run("pause \n")

class TrayGUI():
    """ The GUI running in the tray"""
    def __init__(self):
        config = ConfigParser.ConfigParser()
        configpath = os.path.dirname(__file__) + os.sep + 'radios.conf'
        config.readfp(open(configpath))

        self.mplayer = Mplayer(self)
        self.statusIcon = gtk.StatusIcon()
        menu = gtk.Menu()

        for radio in config.sections() :
            if config.get(radio, 'enabled') != 'True':
                continue

            playlist = False
            name = config.get(radio, 'Name')
            url = config.get(radio, 'url')
            Item = gtk.MenuItem(name)
            if 'type' in config.options(radio):
                if config.get(radio, 'type') == 'playlist':
                    playlist = True

            log("config: radio: {0}, playlist: {1}".format(radio, playlist))

            Item.connect('activate', self.mplayer.radio, url, name, playlist)
            menu.append(Item)

        pause_Item = gtk.MenuItem("Pause")
        pause_Item.connect('activate', self.mplayer.pause, "paused")
        menu.append(pause_Item)

        stop_Item = gtk.MenuItem("Stop")
        stop_Item.connect('activate', self.mplayer.stop, "stoped")
        menu.append(stop_Item)

        self.statusIcon.set_from_file(icon)
        self.statusIcon.set_tooltip("radio")

        self.statusIcon.connect('popup-menu', self.popup_menu_cb_normal, menu)
        self.statusIcon.set_visible(True)

    def quit_callback(self, widget, data = None):
        if data:
            data.set_visible(False)
            self.mplayer.quit()
            reactor.stop()

    def popup_menu_cb_normal(self, widget, button, time, data = None):
        if button == 3 and data:
            data.show_all()
            data.popup(None, None, None, 3, time)

    def run(self):
        reactor.run()

if __name__ == "__main__":
    gui = TrayGUI()
    gui.run()
